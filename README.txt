================================================================================
 INTRODUCTION
================================================================================
This module was originally authored, and is currently maintained by: 
- Zafar Faizi <https://drupal.org/user/1877634>
- Jordan Magnuson <https://drupal.org/user/269983>

Color Tagging allows you to add color tags for image. 
It provides a text field with auto filled with color codes on image upload.

================================================================================
 DEPENDENCIES
================================================================================
This module requires the image module to be installed, which is Drupal core 
module and enabled by default so you don't have to download it.


================================================================================
 INSTALLATION
================================================================================

This module has no special installation requirements other than requiring that 
you first enable the listed dependency module. For general instruction on 
how to install and update Drupal modules see 
<http://drupal.org/getting-started/install-contrib>.

Note: After installing the module flush all of Drupal's caches

================================================================================
 APPLYING COLOR TAGGING TO CONTENT TYPE
================================================================================

  1. Add a new image field to a content type, or use an existing image field and
     configure it t o show the colors elements.
  2. Add or edit a node or any other entity with an image field
  3. Go to the image field on the entity form
  4. Upload an image, colors field will auto populate with colors code.
  5. Save the entity
  6. View the entity to see your image colors
  7. 

================================================================================
 CONFIGURATION
================================================================================

  The configuration is only done on a per field basis.

================================================================================
 COLOR TAGGING THEME
================================================================================

  By default, an image field's colors will be rendered below the image. 
  To customize the image color palette display, copy the color_tagging.tpl.php 
  file to your theme's directory and adjust the html for your needs. 
  Flush Drupal theme registry cache to have it recognize your theme's new file:

  sites/all/themes/MY_THEME/color_tagging.tpl.php

================================================================================
 COLOR TAGGING CSS
================================================================================

  To make changes to the color-tagging css, use this CSS selector:

  .color-tagging-image-palette { /* add custom css here */ }
